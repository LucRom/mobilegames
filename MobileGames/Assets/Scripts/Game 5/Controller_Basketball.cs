using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Controller_Basketball : MonoBehaviour
{
    Rigidbody rb;
    private Vector3 mousePressDownPos;
    private Vector3 mousePressUpPos;
    private bool shooted;

    GameObject[] currentBalls;

    [SerializeField] float forceM;
    [Header("Draw Trayectory")]
    private bool drawTrayectory;
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] float segments;
    private Vector3 forceVector;
    private List<Vector3> linePoints = new List<Vector3>();
    void Start()
    {
        CheckBallsInScene();
        lineRenderer = GameObject.Find("LineTrayectory").GetComponent<LineRenderer>();
        shooted= false;
        rb= GetComponent<Rigidbody>();
        rb.isKinematic = true;
        HideTrayectory();
        drawTrayectory= false;
    }
    private void Update()
    {
       if(Input.GetMouseButtonDown(1))
       {
            drawTrayectory = !drawTrayectory; 
       }
    }
    private void OnMouseDown()
    {
        mousePressDownPos = Input.mousePosition;
    }
    private void OnMouseDrag()
    {
        Vector3 forceInit = (Input.mousePosition - mousePressDownPos);
        forceVector = (new Vector3(forceInit.x, forceInit.y, 0)) * forceM;
        if (shooted == false && drawTrayectory == true)
        {
            Trayectory();
        }
        
    }
    private void OnMouseUp()
    {
        HideTrayectory();
        mousePressUpPos = Input.mousePosition;
        Shoot(mousePressDownPos - mousePressUpPos);

    }
    private void Shoot(Vector3 force)
    {
        if (shooted)
        {
            return;
        }
        rb.isKinematic = false;
        rb.AddForce(new Vector3(force.x, force.y, 0) * forceM);
        shooted=true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Win"))
        {
            Debug.Log("entro");
            GameManager5.point = true;
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("GameOver"))
        {
            GameManager5.gameOver = true;
            Destroy(this.gameObject);
        }
    }
    private void Trayectory()
    {
        Vector3 v = (forceVector / rb.mass) * Time.fixedDeltaTime;
        float tduration = (2 * v.y) / Physics.gravity.y;
        float steptime = tduration / segments;
        linePoints.Clear();
        for(int i=0; i<segments; i++)
        {
            float changeSegment = steptime * i;
            Vector3 movement = new Vector3(v.x * changeSegment, v.y * changeSegment -0.5f * Physics.gravity.y * changeSegment * changeSegment, 0);
            linePoints.Add(-movement + transform.position);
        }
        lineRenderer.positionCount = linePoints.Count;
        lineRenderer.SetPositions(linePoints.ToArray());
    }
    public void HideTrayectory()
    {
        lineRenderer.positionCount = 0;
    }

    private void CheckBallsInScene() 
    {
       currentBalls = GameObject.FindGameObjectsWithTag("Basketball");

        if (currentBalls.Length > 0)
        {
            for (int i = 1; i < currentBalls.Length; i++)
            {
                Destroy(currentBalls[i].gameObject);
                print("destrui un objeto");
            }
        } 
    }
}
