using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager5 : MonoBehaviour
{
    public static bool gameOver;
    public static bool point;
    public GameObject ball;
    public GameObject startpoint;
    public GameObject scoreGO;
    private TextMeshProUGUI scoreTxt;
    public int playerPoints;

    public GameObject hoopGO;
    public List<GameObject> hoopPositions = new List<GameObject>();
    private int randomNumber;

    void Start()
    {
        scoreTxt = scoreGO.GetComponent<TextMeshProUGUI>();
        StartGame();
    }
    void Update()
    {
        if(gameOver)
        {
            playerPoints = 0;
            StartGame();
        }
        if(point)
        {
            playerPoints++;          
            StartGame();
        }
        scoreTxt.text = "Puntos: " + playerPoints.ToString();
    }
    private void StartGame()
    {
        moveHoop();
        Instantiate(ball, startpoint.transform.position, Quaternion.identity);
        point= false;
        gameOver = false;
    }


    private void moveHoop() 
    {
        randomNumber = Random.Range(0, hoopPositions.Count);

        hoopGO.transform.position = hoopPositions[randomNumber].transform.position;
    
    }
}
