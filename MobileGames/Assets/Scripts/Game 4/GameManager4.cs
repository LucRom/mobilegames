using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager4 : MonoBehaviour
{
    public Camera mainCamera;
    public List<GameObject> cubes = new List<GameObject>();
    public static bool gameOver;
    void Start()
    {
        StartGame();
    }
    void Update()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if ((Physics.Raycast(ray, out hit)))
            {
                if(hit.collider.gameObject.CompareTag("Cube"))
                {
                    Controller_Cube controller_Cube = hit.collider.gameObject.GetComponent<Controller_Cube>();
                    controller_Cube.up = !controller_Cube.up;
                }
            }
        }
        if(gameOver)
        {
            StartGame();
        }
    }
    private void StartGame()
    {
        gameOver = false;
        foreach (GameObject cube in cubes)
        {
            cube.GetComponent<Controller_Cube>().up = true;
            cube.transform.position = new Vector3(UnityEngine.Random.Range(-7.89f, 8.29f), UnityEngine.Random.Range(0, 4.85f), 0);
            cube.transform.rotation = Quaternion.Euler(0,0, UnityEngine.Random.Range(-90, 90));
        }
    }
}
