using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Cube : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float force;
    public Material lightBlue;
    public Material orange;
    public bool up;
    void Start()
    {
        up = true;
        rb= GetComponent<Rigidbody>();
    }
    void Update()
    {
        if(up)
        {
            rb.velocity = Vector3.zero;
            GetComponent<MeshRenderer>().material = lightBlue;
            rb.AddForce(Vector3.up * force, ForceMode.Impulse);
        }
        else
        {
            GetComponent<MeshRenderer>().material = orange;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("GameOver") && up==false)
        {
            GameManager4.gameOver= true;
        }
    }
}
