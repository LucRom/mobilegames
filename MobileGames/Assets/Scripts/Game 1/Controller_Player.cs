using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float forceJump;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            rb.AddForce(Vector3.up * forceJump, ForceMode.Impulse);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("GameOver"))
        {
            GameManager1.gameOver = true;
        }
    }
}
