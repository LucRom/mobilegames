using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class Controller_Pipes : MonoBehaviour
{
    public static float speed;
    private void Start()
    {
        speed = 5;
    }
    void Update()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
        if(transform.position.x <= -11.56f)
        {
            Destroy(this.gameObject);
        }
    }
}
