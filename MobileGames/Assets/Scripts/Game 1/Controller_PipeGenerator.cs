using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PipeGenerator : MonoBehaviour
{
    public List<GameObject> pipes = new List<GameObject>();
    [SerializeField] private float timePipe;
    private float time;
    void Start()
    {
        time = timePipe;
    }
    void Update()
    {
        SpawnPipes();
    }
    private void SpawnPipes()
    {
        time -= Time.deltaTime;
        if(time < 0)
        {
            Instantiate(pipes[UnityEngine.Random.Range(0,pipes.Count)], gameObject.transform);
            time = timePipe;
        }
    }
}
