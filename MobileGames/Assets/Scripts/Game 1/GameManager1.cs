using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager1 : MonoBehaviour
{
    public static bool gameOver;
    [SerializeField] GameObject player;
    [SerializeField] GameObject pipeGenerator;
    void Start()
    {
        StartGame();
    }
    void Update()
    {
        if(gameOver)
        {
            StartGame();
        }
    }
    private void StartGame()
    {
        for (var i = pipeGenerator.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(pipeGenerator.transform.GetChild(i).gameObject);
        }

        gameOver = false;
        player.transform.position = new Vector3(-7.74f, 5.97f, 0);
    }
}
