using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller_Menu : MonoBehaviour
{
    public void LevelSelector(string level)
    {
        switch (level)
        {
            case "lvl1":
                SceneManager.LoadScene(1);
                break;
            case "lvl2":
                SceneManager.LoadScene(2);
                break;
            case "lvl3":
                SceneManager.LoadScene(3);
                break;
            case "lvl4":
                SceneManager.LoadScene(4);
                break;
            case "lvl5":
                SceneManager.LoadScene(5);
                break;
            default:
                SceneManager.LoadScene(0);
                break;
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
}
