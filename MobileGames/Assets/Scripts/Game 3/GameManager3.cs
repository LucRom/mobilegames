using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager3 : MonoBehaviour
{
    public static bool gameOver;
    public static bool win;
    [SerializeField] GameObject player;
    public List<GameObject> platforms = new List<GameObject>();
    void Start()
    {
        StartGame();
    }

    void Update()
    {
        if(gameOver || win)
        {
            StartGame();
        }
    }
    
    private void StartGame()
    {
        win = false;
        gameOver = false;
        player.transform.position= new Vector3(0,3,0);
        player.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        foreach(GameObject platform in platforms)
        {
            platform.transform.localRotation= Quaternion.Euler(0,0,0);
        }
    }
}
