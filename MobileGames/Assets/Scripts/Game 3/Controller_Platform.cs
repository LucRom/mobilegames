using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Platform : MonoBehaviour
{
    [SerializeField] float speedRotate;
    void Start()
    {
    }
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 0, speedRotate * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, 0, -speedRotate * Time.deltaTime);
        }
    }
}
