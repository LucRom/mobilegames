using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Ball : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("GameOver"))
        {
            GameManager3.gameOver = true;
        }
        if (collision.gameObject.CompareTag("Win"))
        {
            GameManager3.win = true;
        }
    }
}
