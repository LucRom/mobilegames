using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller_Inputs : MonoBehaviour
{
    private string sceneName;
    private void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            switch (sceneName)
            {
                case "Game1":
                    GameManager1.gameOver = true;
                    break;
                case "Game2":
                    GameManager2.gameOver = true;
                    break;
                case "Game3":
                    GameManager3.gameOver = true;
                    break;
                case "Game4":
                    GameManager4.gameOver = true;
                    break;
                case "Game5":
                    GameManager5.gameOver = true;
                    break;
                default:
                    SceneManager.LoadScene(0);
                    break;
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }

    }
}
