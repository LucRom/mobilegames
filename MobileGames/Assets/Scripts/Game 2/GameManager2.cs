using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager2 : MonoBehaviour
{
    public static bool gameOver;
    public static List<GameObject> boxes = new List<GameObject>(); 
    void Start()
    {
       StartGame();
    }
    void Update()
    {
        if(gameOver)
        {
            StartGame();
        }
    }
    private void StartGame()
    {
        gameOver = false;
        foreach(GameObject box in boxes)
        {
            Destroy(box);
        }
    }
}
