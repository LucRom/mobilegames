using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Box : MonoBehaviour
{
    [SerializeField] bool collided;
    public GameObject sphere;
    void Start()
    {
        collided = false;
        sphere = GameObject.Find("Sphere");
    }
    void Update()
    {
        if(collided == false)
        {
            transform.position += Vector3.right * 5 * Time.deltaTime;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        collided = true;
        if(collision.gameObject.name == "Sphere")
        {
            transform.parent = sphere.transform;
        }
        if (collision.gameObject.CompareTag("GameOver"))
        {
            GameManager2.gameOver = true;
        }
    }
}
