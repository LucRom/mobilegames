using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Sphere : MonoBehaviour
{
    [SerializeField] float speedrotate;
    void Update()
    {
        transform.Rotate(0, 0, speedrotate * Time.deltaTime);
    }
}
