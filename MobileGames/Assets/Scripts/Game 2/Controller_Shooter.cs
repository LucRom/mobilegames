using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Shooter : MonoBehaviour
{
    public GameObject box;
    [SerializeField] GameObject spawnpoint;
    void Start()
    {
        
    }
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            GameObject temp;
            temp = Instantiate(box, spawnpoint.transform.position, Quaternion.identity);
            GameManager2.boxes.Add(temp);
        }
    }
}
